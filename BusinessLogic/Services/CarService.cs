﻿using AutoMapper;
using Domain.Repositores;
using Domain.Resourses;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository repository;
        private readonly IMapper mapper;

        public CarService(ICarRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public CarResource Get(int id)
        {
            var car = repository.Get(id);
            
            var model = mapper.Map<CarResource>(car);
            return model;
        }

        public IList<CarResource> GetAll()
        {
            var cars = repository.GetAll();
            var model = mapper.Map<IList<CarResource>>(cars);
            return model;
        }

    }
}
