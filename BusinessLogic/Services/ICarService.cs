﻿using Domain.Resourses;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Services
{
    public interface ICarService
    {
        IList<CarResource> GetAll();

        CarResource Get(int id);
    }
}
