﻿using AutoMapper;
using CoreWebApi.Resourses;
using Domain.Models;

namespace CoreWebApi.Mapper
{
    public class CarProfile : Profile
    {
        public CarProfile()
        {
            CreateMap<Car, CarResource>();
            CreateMap<CarResource, Car>();
        }
    }
}
