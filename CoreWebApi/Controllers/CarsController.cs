﻿using CoreWebApi.Resourses;
using CoreWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CoreWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CarsController : ControllerBase
    {
        private readonly ICarService service;

        public CarsController(ICarService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IList<CarResource> GetAll()
        {
            return service.GetAll();
        }

        [HttpGet("{id?}")]
        public CarResource Get(int id)
        {
            return service.Get(id);
        }

        [HttpPost]
        public IActionResult Post([FromBody]CarResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Ошибка");
            }

            // добавление Car
            var response = service.Create(resource);
            if (response.IsSuccess) return Ok("Added");

            return BadRequest(response.ErrorMessage);
        }

        [HttpPut("{id?}")]
        public IActionResult Put(int id, [FromBody] CarResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Ошибка");
            }

            var response = service.Update(id, resource);

            if (response.IsSuccess)
            {
                return Ok("Обновлен");
            }
            return BadRequest(response.ErrorMessage);
        }

        // Query String - Строка запроса

        [HttpDelete("{id}")]
        //[HttpGet("[action]/{id}")]
        public IActionResult Delete(int id)
        {
            var response = service.Delete(id);
            if (response.IsSuccess) return Ok(response.Car);
            return BadRequest(response.ErrorMessage);
        }
    }
}
