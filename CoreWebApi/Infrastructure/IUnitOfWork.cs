﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebApi.Infrastructure
{
    public interface IUnitOfWork
    {
        public void BeginTransaction();
        public void Save();
        public void Commit();
        public void Rollback();

    }
}
