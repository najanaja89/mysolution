﻿using Domain.DbContext;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebApi.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CarDbContext _carDbContext;
        private IDbContextTransaction _transaction;

        public UnitOfWork(CarDbContext carDbContext)
        {
            _carDbContext = carDbContext;
        }

        public void BeginTransaction()
        {
            _transaction = _carDbContext.Database.BeginTransaction();
        }
        public void Save()
        {
            _carDbContext.SaveChanges();
        }
        

        public void Commit()
        {
            if (_transaction==null)
            {
                return;
            }
            _transaction.Commit();
        }

        public void Rollback()
        {
            if (_transaction == null)
            {
                return;
            }
            _transaction.Rollback();
        }
    }
}
