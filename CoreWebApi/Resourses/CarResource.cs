﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CoreWebApi.Resourses
{
    public class CarResource
    {
        public int Id { get; set; }

        [Required]
        public string Brand { get; set; }

        public Kuzov Kuzov { get; set; }

        [MaxLength(20)]
        public string Color { get; set; }
    }
}
