﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebApi.Responses
{
    public class CarResponse
    {
        public Car Car { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        private CarResponse(Car car, bool isSuccess, string errorMessage)
        {
            Car = car;
            IsSuccess = isSuccess;
            ErrorMessage = errorMessage;
        }

        public CarResponse(Car car) :this(car, true,"")
        {
                
        }

        public CarResponse(string errorMessage) :this(null, false, errorMessage)
        {

        }
    }
}
