﻿using CoreWebApi.Resourses;
using CoreWebApi.Responses;
using System.Collections.Generic;

namespace CoreWebApi.Services
{
    public interface ICarService
    {
        IList<CarResource> GetAll();

        CarResource Get(int id);

        CarResponse Create(CarResource resource);

        CarResponse Update(int id, CarResource resource);

        CarResponse Delete(int id);
    }
}
