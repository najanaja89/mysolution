﻿using AutoMapper;
using CoreWebApi.Infrastructure;
using CoreWebApi.Resourses;
using CoreWebApi.Responses;
using Domain.Models;
using Domain.Repositores;
using System.Collections.Generic;

namespace CoreWebApi.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository _repository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CarService(ICarRepository repository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this._repository = repository;
            this._mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public CarResponse Create(CarResource resource)
        {
            _unitOfWork.BeginTransaction();
            try
            {
                var car = _mapper.Map<Car>(resource);
                var newCar = _repository.Create(car);
                _unitOfWork.Save();
                _unitOfWork.Commit();
                return new CarResponse(newCar);
            }
            catch (System.Exception e)
            {
                _unitOfWork.Rollback();
                return new CarResponse("Save error:" + e.Message);
            }

            //1. Пытаемся сохранить
        }

        public CarResponse Delete(int id)
        {
            _unitOfWork.BeginTransaction();
            try
            {
                var car = _repository.Get(id);
                car = _repository.Delete(car);
                _unitOfWork.Save();
                _unitOfWork.Commit();
                return new CarResponse(car);
              
            }
            catch (System.Exception e)
            {
                _unitOfWork.Rollback();
                return new CarResponse("Delete error:" + e.Message);
            }
        }


        public CarResource Get(int id)
        {
            var car = _repository.Get(id);
            var model = _mapper.Map<CarResource>(car);
            return model;
        }

        public IList<CarResource> GetAll()
        {
            var cars = _repository.GetAll();
            var model = _mapper.Map<IList<CarResource>>(cars);
            return model;
        }

        public CarResponse Update(int id, CarResource resource)
        {
            _unitOfWork.BeginTransaction();            
            try
            {
                var car = _repository.Get(id);

                car.Brand = resource.Brand;
                car.Kuzov = resource.Kuzov;
                car.Color = resource.Color;

                //car = mapper.Map<Car>(resource);

                car = _repository.Update(car);
                _unitOfWork.Save();
                _unitOfWork.Commit();
                return new CarResponse(car);
            }
            catch (System.Exception e)
            {
                _unitOfWork.Rollback();
                return new CarResponse("Edit error:" + e.Message);
            }

        }
    }


}

