﻿using Domain.Models;
using System.Collections.Generic;

namespace Domain.Repositores
{
    public interface ICarRepository
    {
        Car Get(int id);
        IList<Car> GetAll();
        Car Create(Car car);
        Car Update(Car car);
        Car Delete(Car car);
    }
}
