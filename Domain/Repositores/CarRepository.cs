﻿using System.Collections.Generic;
using System.Linq;
using Domain.DbContext;
using Domain.Models;

namespace Domain.Repositores
{
    public class CarRepository : ICarRepository
    {
        private readonly CarDbContext db;

        public CarRepository(CarDbContext db)
        {
            this.db = db;
        }

        public Car Create(Car car)
        {
            var result = db.Cars.Add(car);
            //db.SaveChanges();

            return result.Entity;
        }

        public Car Delete(Car car)
        {
         
                db.Cars.Remove(car);
                //db.SaveChanges();
                return car;    
        }

        public Car Get(int id)
        {
            var car = db.Cars.FirstOrDefault(c => c.Id == id);
            return car;
        }

        public IList<Car> GetAll()
        {
            return db.Cars.ToList();
        }

        public Car Update(Car car)
        {
            var result = db.Cars.Update(car);
            //db.SaveChanges();
            return result.Entity;
        }
    }
}
