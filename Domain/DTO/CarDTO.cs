﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class CarDto
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public Kuzov Kuzov { get; set; }
        public string Color { get; set; }
    }
}
