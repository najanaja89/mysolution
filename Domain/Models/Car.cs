﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public Kuzov Kuzov { get; set; }
        public string Color { get; set; }
    }
}
