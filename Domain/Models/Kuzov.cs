﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public enum Kuzov
    {
        Sedan,
        Hatchback,
        Kupe,
        Cabrio,
        Roadster
    }
}
