﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Domain.DbContext
{
    public class CarDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public virtual DbSet<Car> Cars { get; set; }

        public CarDbContext(DbContextOptions<CarDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Car>().HasData(
                new Car { Id = 1, Brand = "Lamborgini", Kuzov = Kuzov.Cabrio, Color = "red" },
                new Car { Id = 2, Brand = "Maserati", Kuzov = Kuzov.Kupe, Color = "green" },
                new Car { Id = 3, Brand = "BMW", Kuzov = Kuzov.Sedan, Color = "blue" });
        }
    }
}
